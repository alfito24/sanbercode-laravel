<?php

namespace App\Http\Middleware;

use App\users;
use Closure;

class verified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(users::email_verified() !== null && users::password() !== null){
            return $next($request);
        }
        return abort(403);

    }
}
