<?php

namespace App\Http\Middleware;

use App\users;
use Closure;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, users $users)
    {
        if($users->roles()->name() == 'admin'){
            return $next($request);
        }
        return abort(403);
    }
}
